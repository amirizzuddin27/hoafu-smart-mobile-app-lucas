import { Component, OnInit } from '@angular/core';
import { TemplatesService } from '../templates.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-edit-template',
  templateUrl: './edit-template.component.html',
  styleUrls: ['./edit-template.component.css']
})
export class EditTemplateComponent implements OnInit {

  public selectJobcard;
  public templateData = [];
  public templateWorksheetCode;
  public Data;
  public partNumber = [];

  public idPartNumber = [];
  public idTaskType = [];
  public idJobcard = [];
  public idWorksheetJobcard = [];
  public idJobcardTask = [];
  public idTask = [];

  _url: string = 'https://haofu.airbusdigital.com/api/hof/template/worksheet';

  templateForm: FormGroup;

  data = {
    templatejobcard: [
      {
        templatejobcardtitle: "",
        templatejobcarddescription: "",
        templatejobcardremarks: "",
        templatetask: [
          {
            templatetasktitle: "",
            templatetaskremarks: "",
            taskregulationid: "",
            childtemplatetask: [
              {
                templatetasktitle: "",
                templatetaskremarks: "",
                taskregulationid: "",
            }],
            templatefinding: [
              {
                templatefindingtitle: "",
                templatefindingrawdata: "",
            }]
        }]
    }]
  }

  constructor(private http: HttpClient, private _templatesService: TemplatesService, private route: ActivatedRoute, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {

    var code = this.route.snapshot.paramMap.get('templateworksheetcode');
    this.templateWorksheetCode = code;

    this._templatesService.getTemplateData(this.templateWorksheetCode)
      .subscribe(
        response => {
          this.Data = response['Result'];
          this.templateData = this.Data[0];
          this.placeData(this.templateData);
      }
    )

    this.templateForm = this.fb.group({
      templateworksheetid: [null],
      templateworksheetcode: [null],
      templateworksheettitle: [null],
      templateworksheetdescription: [null],
      templateworksheetmanufacturer: [null],
      part: this.fb.array([]),
      templateworksheetformno: [null],
      templateworksheetformrevisionno: [null],
      templateworksheetformrevisiondate: [null],
      templateworksheetformrevisionreason: [null],
      templateworksheettasktype: this.fb.array([]),
      templatejobcard: this.fb.array([]),
    });

    this.setJobcards();
    this.deleteFirstJobcard(0);

    this.idJobcard = [];
    this.idWorksheetJobcard = [];
    console.log(this.idJobcard)
  }

  placeData(data) {
    var i,j,k,l;
    var control, jobcardData, taskData, fintask;
    this.templateForm.patchValue({
      templateworksheetid: data.templateworksheetid,
      templateworksheetcode: data.templateworksheetcode,
      templateworksheettitle: data.templateworksheettitle,
      templateworksheetdescription: data.templateworksheetdescription,
      templateworksheetmanufacturer: data.templateworksheetmanufacturer,
      templateworksheetformno: data.templateworksheetformno,
      templateworksheetformrevisionno: data.templateworksheetformrevisionno,
      templateworksheetformrevisiondate: data.templateworksheetformrevisiondate,
      templateworksheetformrevisionreason: data.templateworksheetformrevisionreason,
    });

    if (data.templateworksheetpartids != null) {
      for (i=0; i<(data.part.length); i++) {
        (<FormArray>this.templateForm.controls.part).push(
          this.fb.group({
            templateworksheetpartid: [data.part[i].templateworksheetpartid],
            partnumber: [data.part[i].partnumber],
            partnomenclature: [data.part[i].partnomenclature],
          })
        )
      }
    }

    if (data.templateworksheettasktypeids != null) {
      for (i=0; i<(data.templateworksheettasktype.length); i++) {
        (<FormArray>this.templateForm.controls.templateworksheettasktype).push(
          this.fb.group({
            templateworksheettasktypeid: [data.templateworksheettasktype[i].templateworksheettasktypeid],
            templateworksheettasktype: [data.templateworksheettasktype[i].templateworksheettasktype],
          })
        )
      }
    }
    if (data.templatejobcardids != null) {
      for (i=0; i<data.templatejobcard.length; i++) {
        (<FormArray>this.templateForm.controls.templatejobcard).push(
          this.fb.group({
            templateworksheetjobcardid: [data.templatejobcard[i].templateworksheetjobcardid],
            templatejobcardid: [data.templatejobcard[i].templatejobcardid],
            templatejobcardtitle: [data.templatejobcard[i].templatejobcardtitle],
            templatejobcarddescription: [data.templatejobcard[i].templatejobcarddescription],
            templatejobcardremarks: [data.templatejobcard[i].templatejobcardremarks],
            templatetask: this.fb.array([])
          })
        )
        jobcardData = this.templateForm.controls.templatejobcard;
        control = jobcardData.controls[i].controls.templatetask;
        if (data.templatejobcard[i].templatetaskids != null) {
          for(j=0; j<data.templatejobcard[i].templatetask.length; j++) {
            (control).push(
              this.fb.group({
                templatejobcardtaskid: [data.templatejobcard[i].templatetask[j].templatejobcardtaskid],
                templatetaskid: [data.templatejobcard[i].templatetask[j].templatetaskid],
                templatetasktitle: [data.templatejobcard[i].templatetask[j].templatetasktitle],
                templatetaskremarks: [data.templatejobcard[i].templatetask[j].templatetaskremarks],
                taskregulationid: [data.templatejobcard[i].templatetask[j].taskregulationid],
                childtemplatetask: this.fb.array([]),
                templatefinding: this.fb.array([])
              })
            )
            taskData = control.controls[j].controls.childtemplatetask;
            if (data.templatejobcard[i].templatetask[j].childtemplatetask != null) {
              for(k=0; k<data.templatejobcard[i].templatetask[j].childtemplatetask.length; k++) {
                taskData.push(
                  this.fb.group({
                    templatetaskid: [data.templatejobcard[i].templatetask[j].childtemplatetask[k].templatetaskid],
                    templatetasktitle: [data.templatejobcard[i].templatetask[j].childtemplatetask[k].templatetasktitle],
                    templatetaskremarks: [data.templatejobcard[i].templatetask[j].childtemplatetask[k].templatetaskremarks],
                    taskregulationid: [data.templatejobcard[i].templatetask[j].childtemplatetask[k].taskregulationid]
                  })
                )
              }
            }
            fintask = control.controls[j].controls.templatefinding;
            if (data.templatejobcard[i].templatetask[j].templatefindingids != null) {
              for (l=0; l<data.templatejobcard[i].templatetask[j].templatefinding.length; l++)
                fintask.push(
                  this.fb.group({
                    templatefindingids: [data.templatejobcard[i].templatetask[j].templatefinding[l].templatefindingids],
                    templatetaskfindingid: [data.templatejobcard[i].templatetask[j].templatefinding[l].templatetaskfindingid],
                    templatefindingtitle: [data.templatejobcard[i].templatetask[j].templatefinding[l].templatefinding[0].templatefindingtitle],
                    templatefindingrawdata: [data.templatejobcard[i].templatetask[j].templatefinding[l].templatefinding[0].templatefindingrawdata]
                })
              )
            }
          }
        }
      }
    }
  };

  addPartNumber() {
    let control = <FormArray>this.templateForm.controls.part;
    control.push(
      this.fb.group({
        partnumber: [null],
        partnomenclature: ['RADOME'],
      })
    )
  }

  addTaskType() {
    let control = <FormArray>this.templateForm.controls.templateworksheettasktype;
    control.push(
      this.fb.group({
        templateworksheettasktype: [null],
      })
    )
  }

  addNewJobcard() {
    let control = <FormArray>this.templateForm.controls.templatejobcard;
    control.push(
      this.fb.group({
        templatejobcardtitle: [null],
        templatejobcarddescription: [null],
        templatejobcardremarks: [null],
        templatetask: this.fb.array([])
      })
    )
    this.showJC();
  }

  addNewTask(control) {
    control.push(
      this.fb.group({
        templatetasktitle: [null],
        templatetaskremarks: [null],
        taskregulationid: ['default'],
        childtemplatetask: this.fb.array([]),
        templatefinding: this.fb.array([])
      })
    )
  }

  addNewSubtask(task, control) {
    control.push(
      this.fb.group({
        templatetasktitle: [null],
        templatetaskremarks: [null],
        taskregulationid: ['default'],
      }))
      var element, index;
      element = task.controls.taskregulationid.value;
      index = control.length-1
      if (element == 'default') { control.controls[index].patchValue({taskregulationid: 'default'})
      }
      if (element=='1') { control.controls[index].patchValue({taskregulationid: '1'})
      }
      if (element=='2') { control.controls[index].patchValue({taskregulationid: '2'})
      }
  }

  addNewFindingTask(control) {
    control.push(
      this.fb.group({
        templatefindingtitle: [''],
        templatefindingrawdata: ['']
      })
    )
  }

  setJobcards() {
    let control = <FormArray>this.templateForm.controls.templatejobcard;
    this.data.templatejobcard.forEach(x => {
      control.push(this.fb.group({
        templatejobcardtitle: x.templatejobcardtitle,
        templatejobcarddescription: x.templatejobcarddescription,
        templatejobcardremarks: x.templatejobcardremarks,
        templatetask: this.setTasks(x)
      }))
    })
  }

  setTasks(x) {
    let arr = new FormArray([])
    x.templatetask.forEach(y => {
      arr.push(this.fb.group({
        templatetasktitle: y.templatetasktitle,
        templatetaskremarks: y.templatetaskremarks,
        taskregulationid: y.taskregulationid,
        childtemplatetask: this.setSubtasks(y),
        // templatefinding: this.setFindingTask(y)
      }))
    })
    return arr;
  }

  setSubtasks(y) {
    let arr = new FormArray([])
    y.childtemplatetask.forEach(z => {
      arr.push(this.fb.group({
        templatetasktitle: z.templatetasktitle,
        templatetaskremarks: z.templatetaskremarks,
        taskregulationid: z.taskregulationid
      }))
    })
    return arr;
  }

  setFindingTask(y) {
    let fin = new FormArray([])
    y.templatefinding.forEach(b => {
      fin.push(this.fb.group({
        templatefindingtitle: b.templatefindingtitle,
        templatefindingrawdata: b.templatefindingrawdata
      }))
    })
    return fin;
  }

  deletePartNumber(index) {
    let control = <FormArray>this.templateForm.controls.part;
    this.idPartNumber.push(this.templateForm.value.part[index].templateworksheetpartid);
    control.removeAt(index)
  }

  deleteTaskType(index) {
    let control = <FormArray>this.templateForm.controls.templateworksheettasktype;
    this.idTaskType.push(this.templateForm.value.templateworksheettasktype[index].templateworksheettasktypeid);
    control.removeAt(index)
  }

  deleteFirstJobcard(index) {
    let control = <FormArray>this.templateForm.controls.templatejobcard;
    control.removeAt(index);
    this.showJC();
  }

  deleteJobcard(index) {
    var i, j;
    var dataJobcard, dataTask;
    let control = <FormArray>this.templateForm.controls.templatejobcard;

    this.idWorksheetJobcard.push(this.templateForm.value.templatejobcard[index].templateworksheetjobcardid);
    this.idJobcard.push(this.templateForm.value.templatejobcard[index].templatejobcardid);

    this._templatesService.getJobcard(this.templateForm.value.templatejobcard[index].templatejobcardid)
    .subscribe(
      response => {dataJobcard = response['Result'][0];
      if (dataJobcard.templatetaskids != null) {
        for (i=0; i<dataJobcard.templatetaskids.length; i++) {
          this.sameNumber(this.idJobcardTask, dataJobcard.templatejobcardtaskids[i]);
          this.sameNumber(this.idTask, dataJobcard.templatetaskids[i]);
          this._templatesService.getTask(dataJobcard.templatetaskids[i])
          .subscribe(
            response => { dataTask = response['Result'][0];
            if (dataTask.childtemplatetaskids != null) {
              for (j=0; j<dataTask.childtemplatetaskids.length; j++) {
                this.sameNumber(this.idTask, dataTask.childtemplatetaskids[j])
              }
            }
            }
          )
        }
      }
    }
    )
    control.removeAt(index);
    this.showJC();
  }

  deleteTask(control, index) {
    var i;
    var dataTask;
    let tasks = control.value;
    this.idJobcardTask.push(tasks[index].templatejobcardtaskid);
    this.idTask.push(tasks[index].templatetaskid);

    this._templatesService.getTask(tasks[index].templatetaskid)
    .subscribe(
      response => { dataTask = response['Result'][0];
      if (dataTask.childtemplatetaskids != null) {
        for (i=0; i<dataTask.childtemplatetaskids.length; i++) {
          this.sameNumber(this.idTask, dataTask.childtemplatetaskids[i])
        }
      }
      }
    )
    control.removeAt(index)
  }


  deleteSubtask(control, index) {
    let subtasks = control.value;
    this.idTask.push(subtasks[index].templatetaskid);
    control.removeAt(index)
  }

  deleteFindingTask(control, index) {
    control.removeAt(index)
  }

  showJC() {

    var liste, texte, i, index;
    liste = document.getElementById("nbJobcards");
    index = liste.length-1;
    texte = liste.selectedIndex-1;
    for (i=0; i<index; i++) {
      if(texte == i) {
        document.getElementById(texte).style.display = 'block';
      } else {
        document.getElementById(i).style.display = 'none';
      }
    }
  }


  taskRegulation(control, index) {
    var element, subtaskRegulation, j;
    element = control.controls[index].controls.taskregulationid.value;
    subtaskRegulation = control.controls[index].controls.childtemplatetask

    for(j=0; j<subtaskRegulation.length; j++) {
      if (element=='default') {
        subtaskRegulation.controls[j].patchValue({taskregulationid: 'Task Regulation'})
      }
      if (element=='1') {
      subtaskRegulation.controls[j].patchValue({taskregulationid: '1'})
      }
      if (element=='2') {
        subtaskRegulation.controls[j].patchValue({taskregulationid: '2'})
        }
      }
  }


  onSubmit() {
    var i;
    // console.log('idPartNumber = ', this.idPartNumber)
    // console.log('idTaskType = ', this.idTaskType)
    // console.log('idJobcard = ', this.idJobcard)
    // console.log('idWorksheetJobcard = ', this.idWorksheetJobcard)
    // console.log('idJobcardTask = ', this.idJobcardTask)
    // console.log('idTask = ', this.idTask)
  
    for (i=0; i<this.idPartNumber.length; i++) {
      this._templatesService.deletePartNumber(this.idPartNumber[i])
      .subscribe(
        response => { console.log(response) }
      );  
    }

    for (i=0; i<this.idTaskType.length; i++) {
      this._templatesService.deleteTaskType(this.idTaskType[i])
      .subscribe(
        response => { console.log(response) }
      );
    }

    for (i=0; i<this.idWorksheetJobcard.length; i++) {
      this._templatesService.removeJobcard(this.idWorksheetJobcard[i])
      .subscribe(
        response => { console.log(response) }
      );
    }

    for (i=0; i<this.idJobcard.length; i++) {
      this._templatesService.deleteJobcard(this.idJobcard[i])
      .subscribe(
        response => { console.log(response) }
      );
    }

    for (i=0; i<this.idJobcardTask.length; i++) {
      this._templatesService.removeTask(this.idJobcardTask[i])
      .subscribe(
        response => { console.log(response) }
      );
    }

    for (i=0; i<this.idTask.length; i++) {
      this._templatesService.deleteTask(this.idTask[i])
      .subscribe(
        response => { console.log(response) }
      );
    }


    return this.http.patch(this._url, [this.templateForm.value])
    .subscribe(
      (data:any) => {
        console.log(data)
      }
    )
  }


  sameNumber(group, value) {
    var i;
    for(i=0; i<group.length; i++) {
      if (group[i]==value) {
        return group;
      }
    }
    group.push(value)
  }

}