import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplatesComponent } from './templates/templates.component';
import { AddTemplateComponent } from './add-template/add-template.component';
import { TemplatesService } from './templates.service';
import { EditTemplateComponent } from './edit-template/edit-template.component';

import {MatNativeDateModule} from '@angular/material/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DemoMaterialModule} from '../material-module';
import { TemplatesDialogComponent } from './templates/templates.component';


@NgModule({
  declarations: [
    AppComponent,
    TemplatesComponent,
    AddTemplateComponent,
    EditTemplateComponent,
    TemplatesDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    MatNativeDateModule
  ],
  
  providers: [AppComponent, TemplatesService],
  bootstrap: [AppComponent],
  entryComponents: [TemplatesComponent, TemplatesDialogComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);