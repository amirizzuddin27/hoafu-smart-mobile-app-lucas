import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { TemplatesService } from '../templates.service';
import { Router } from '@angular/router';
import { pipe } from 'rxjs';
import { componentFactoryName } from '@angular/compiler';

export interface DialogData {
  ReferenceNumber: string;

}

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {

  ReferenceNumber: string;

  public templates;

  worksheetId: string;
  public idPartNumber = [];
  public idTaskType = [];
  public idJobcard = [];
  public idWorksheetJobcard = [];
  public idTask = [];
  public idTaskTotal = [];
  public idJobcardTask = [];

  constructor(private _templatesService: TemplatesService, private router: Router, public dialog: MatDialog) { }

  ngOnInit() { 
    this._templatesService.getTemplates()
      .subscribe(
        response => {
          this.templates = response['Result'] 
        }
      );  
  }

  onSelect(template) {
    this.router.navigate(['/templates', template.templateworksheetcode])
  }

  openDialog(refnum): void {
    var i;
    this.ReferenceNumber = refnum;
    const dialogRef = this.dialog.open(TemplatesDialogComponent, {
      width: '400px',
      data: {ReferenceNumber: refnum}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      if (result == this.ReferenceNumber) {
        console.log('idPartNumber = ', this.idPartNumber)
        console.log('idTaskType = ', this.idTaskType)
        console.log('idJobcard = ', this.idJobcard)
        console.log('idWorksheetJobcard = ', this.idWorksheetJobcard)
        console.log('idTask = ', this.idTask)
        console.log('idJobcardTask = ', this.idJobcardTask)

        if (this.idPartNumber != null) {
          for (i=0; i<this.idPartNumber.length; i++) {
            console.log(this.idPartNumber[i])
            this._templatesService.deletePartNumber(this.idPartNumber[i])
            .subscribe(
              response => { console.log(response) }
            ); 
          }
          if (i == this.idPartNumber.length-1) {
            this.idPartNumber=[]
            console.log(this.idPartNumber)
          }
        }
        
        if (this.idTaskType != null) {
          for (i=0; i<this.idTaskType.length; i++) {
            console.log(this.idTaskType[i])
            this._templatesService.deleteTaskType(this.idTaskType[i])
            .subscribe(
              response => { console.log(response) }
            );
          }
          if (i == this.idTaskType.length-1) {
            this.idTaskType=[]
            console.log(this.idTaskType)
          }
        }

        if (this.idJobcardTask != null) {
          for (i=0; i<this.idJobcardTask.length; i++) {
            console.log(this.idJobcardTask[i])
            this._templatesService.removeTask(this.idJobcardTask[i])
            .subscribe(
              response => {console.log(response) }
            );
          }
          if (i == this.idJobcardTask.length-1) {
            this.idJobcardTask=[]
          }
        }

        if (this.idTask != null) {
          for (i=0; i<this.idTask.length; i++) {
            console.log(this.idTask[i])
            this._templatesService.deleteTask(this.idTask[i])
            .subscribe(
              response => {console.log(response) }
            );
          }
          if (i == this.idTask.length-1) {
            this.idTask=[]
          }
        }

        if (this.idWorksheetJobcard != null) {
          for (i=0; i<this.idWorksheetJobcard.length; i++) {
            console.log(this.idWorksheetJobcard[i])
            this._templatesService.removeJobcard(this.idWorksheetJobcard[i])
            .subscribe(
              response => {console.log(response) }
            );
          }
          if (i == this.idWorksheetJobcard.length-1) {
            this.idWorksheetJobcard=[]
          }
        }

        if (this.idJobcard != null) {
          for (i=0; i<this.idJobcard.length; i++) {
            console.log(this.idJobcard[i])
            this._templatesService.deleteJobcard(this.idJobcard[i])
            .subscribe(
              response => {console.log(response) }
            );
          }
          if (i == this.idJobcard.length-1) {
            this.idJobcard=[]
          }
        }

        this._templatesService.deleteTemplate(this.worksheetId)
        .subscribe(
          response => {console.log(response) 
          }
        )

      }
    });
  }






  deleteTemplate(template) {
    var i, j;

    this.worksheetId = template.templateworksheetid;

    this.idPartNumber = template.templateworksheetpartids;

    this.idTaskType = template.templateworksheettasktypeids;

    this.idWorksheetJobcard = template.templateworksheetjobcardids;

    this.idJobcard = template.templatejobcardids;

    if (this.idJobcard != null) {
      for (i=0; i<this.idJobcard.length; i++) {
        this._templatesService.getJobcard(this.idJobcard[i])
        .subscribe(
          response => { 
            if(response['Result'] != '') {
              if (response['Result'][0].templatetaskids != null) {
                this.fusionGroup(this.idTask, response['Result'][0].templatetaskids);
                this.fusionGroup(this.idJobcardTask, response['Result'][0].templatejobcardtaskids)
                if (this.idTask != null) {
                  for (j=0; j<this.idTask.length; j++) {
                    this._templatesService.getTask(this.idTask[j])
                    .subscribe(
                      response => {
                        if (response['Result'][0].childtemplatetaskids != null) {
                          this.fusionGroup(this.idTaskTotal, response['Result'][0].childtemplatetaskids)
                          this.fusionGroup(this.idTask, this.idTaskTotal)
                        }
                      }
                    )
                  }
                }
              }
            }
          }
        )
      }
    }

  }

  fusionGroup(group1, group2) {
    var i;
    if (group2 != null) {
      for (i=0; i<group2.length; i++) {
        this.sameNumber(group1, group2[i])
      }
    }
    return group1
  }

  sameNumber(group, value) {
    var i;
    for(i=0; i<group.length; i++) {
      if (group[i]==value) {
        return group;
      }
    }
    group.push(value)
  }


}


@Component({
  selector: 'templates-dialog.component',
  templateUrl: 'templates-dialog.component.html',
})

export class TemplatesDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<TemplatesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
