import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplatesComponent } from './templates/templates.component';
import { AddTemplateComponent } from './add-template/add-template.component';
import { EditTemplateComponent } from './edit-template/edit-template.component';

const routes: Routes = [
   { path: '', redirectTo: '/templates', pathMatch: 'full'},
   { path: 'templates', component: TemplatesComponent},
   { path: 'templates/:templateworksheetcode', component: EditTemplateComponent},
   { path: 'add-template', component: AddTemplateComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [TemplatesComponent,
                                  AddTemplateComponent,
                                  EditTemplateComponent
                                  ]