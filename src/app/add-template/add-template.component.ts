import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-template',
  templateUrl: './add-template.component.html',
  styleUrls: ['./add-template.component.css']
})
export class AddTemplateComponent implements OnInit {

  _url: string = 'https://haofu.airbusdigital.com/api/hof/template/worksheet';
  
  templateForm: FormGroup;

  data = {
    templatejobcard: [
      {
        templatejobcardtitle: "",
        templatejobcarddescription: "",
        templatejobcardremarks: "",
        templatetask: [
          {
            templatetasktitle: "",
            templatetaskremarks: "",
            taskregulationid: "",
            childtemplatetask: [
              {
                templatetasktitle: "",
                templatetaskremarks: "",
                taskregulationid: "",
            }],
            templatefinding: [
              {
                templatefindingtitle: "",
                templatefindingrawdata: "",
              }
            ]
        }]
    }]
  }


  constructor(private router: Router, private route: ActivatedRoute, private fb: FormBuilder, private http: HttpClient) { }

  public selectJobcard;

  ngOnInit() {
    this.templateForm = this.fb.group({
      templateworksheetcode: [null],
      templateworksheettitle: [null],
      templateworksheetdescription: [null],
      templateworksheetmanufacturer: [null],
      part: this.fb.array([]),
      templateworksheetformno: [null],
      templateworksheetformrevisionno: [null],
      templateworksheetformrevisiondate: [null],
      templateworksheetformrevisionreason: [null],
      templateworksheettasktype: this.fb.array([]),
      templatejobcard: this.fb.array([]),
    });
  
    this.setJobcards();
    this.addPartNumber();
    this.addTaskType();
    this.deleteJobcard(0);
  }


  addPartNumber() {
    let control = <FormArray>this.templateForm.controls.part;
    control.push(
      this.fb.group({
        partnumber: [null],
        partnomenclature: ['RADOME']
      })
    )
  }

  addTaskType() {
    let control = <FormArray>this.templateForm.controls.templateworksheettasktype;
    control.push(
      this.fb.group({
        templateworksheettasktype: [null],
      })
    )
  }

  onSelect(jobcard){
    this.router.navigate([jobcard.value], {relativeTo: this.route});
  }

  addNewJobcard() {
    let control = <FormArray>this.templateForm.controls.templatejobcard;
    control.push(
      this.fb.group({
        templatejobcardtitle: [null],
        templatejobcarddescription: [null],
        templatejobcardremarks: [null],
        templatetask: this.fb.array([])
      })
    )
    this.showJC();
  }

  addNewTask(control) {
    control.push(
      this.fb.group({
        templatetasktitle: [null],
        templatetaskremarks: [null],
        taskregulationid: ['default'],
        childtemplatetask: this.fb.array([]),
        templatefinding: this.fb.array([])
      })
    )
  }

  addNewSubtask(task, control) {
    control.push(
      this.fb.group({
        templatetasktitle: [null],
        templatetaskremarks: [null],
        taskregulationid: ['default'],
        // templatefinding: this.fb.array([])
      }))
      var element, index;
      element = task.controls.taskregulationid.value;
      index = control.length-1
      if (element == 'default') { control.controls[index].patchValue({taskregulationid: 'default'})
      } 
      if (element=='1') { control.controls[index].patchValue({taskregulationid: '1'})
      }   
      if (element=='2') { control.controls[index].patchValue({taskregulationid: '2'})
      }   
  }

  // addNewFindingSubtask(control) {
  //   control.push(
  //     this.fb.group({
  //       templatefindingtitle: [''],
  //     })
  //   )
  // }

  addNewFindingTask(control) {
    control.push(
      this.fb.group({
        templatefindingtitle: [null],
        templatefindingrawdata: [null]
      })
    )
  }

  setJobcards() {
    let control = <FormArray>this.templateForm.controls.templatejobcard;
    this.data.templatejobcard.forEach(x => {
      control.push(this.fb.group({
        templatejobcardtitle: x.templatejobcardtitle,
        templatejobcarddescription: x.templatejobcarddescription,
        templatejobcardremarks: x.templatejobcardremarks,
        templatetask: this.setTasks(x)
      }))
    })
  }

  setTasks(x) {
    let arr = new FormArray([])
    x.templatetask.forEach(y => {
      arr.push(this.fb.group({
        templatetasktitle: y.templatetasktitle,
        templatetaskremarks: y.templatetaskremarks,
        taskregulationid: y.taskregulationid,
        childtemplatetask: this.setSubtasks(y),
        templatefinding: this.setFindingTask(y)
      }))
    })
    return arr;
  }

  setSubtasks(y) {
    let arr = new FormArray([])
    y.childtemplatetask.forEach(z => {
      arr.push(this.fb.group({
        templatetasktitle: z.templatetasktitle,
        templatetaskremarks: z.templatetaskremarks,
        taskregulationid: z.taskregulationid,
        // templatefinding: this.setFindingSubtask(z)
      }))
    })
    return arr;
  }

  // setFindingSubtask(y) {
  //   let fins = new FormArray([])
  //   y.templatefinding.forEach(z => {
  //     fins.push(this.fb.group({
  //       templatefindingtitle: z.templatefindingtitle,
  //     }))
  //   })
  //   return fins;
  // }

  setFindingTask(y) {
    let fin = new FormArray([])
    y.templatefinding.forEach(b => {
      fin.push(this.fb.group({
        templatefindingtitle: b.templatefindingtitle,
        templatefindingrawdata: b.templatefindingrawdata
      }))
    })
    return fin;
  }

  deletePartNumber(index) {
    let control = <FormArray>this.templateForm.controls.part;
    control.removeAt(index)
  }

  deleteTaskType(index) {
    let control = <FormArray>this.templateForm.controls.templateworksheettasktype;
    control.removeAt(index)
  }

  deleteJobcard(index) {
    let control = <FormArray>this.templateForm.controls.templatejobcard;
    control.removeAt(index);
    this.showJC();
  }

  deleteTask(control, index) {
    console.log((control.value)[index].taskregulationid)
    control.removeAt(index)
  }

  deleteSubtask(control, index) {
    control.removeAt(index)
  }

  // deleteFindingSubtask(control, index) {
  //   control.removeAt(index)
  // }

  deleteFindingTask(control, index) {
    control.removeAt(index)
  }

  showJC() {
      var liste, texte, i, index;
      liste = document.getElementById("nbJobcards");
      index = liste.length-1;
      texte = liste.selectedIndex-1;
      for (i=0; i<index; i++) {
        if(texte == i) {
          document.getElementById(texte).style.display = 'block';
        } else {
          document.getElementById(i).style.display = 'none';
        }
      }
  }

  taskRegulation(control, index) {
    var element, subtaskRegulation, j;
    element = control.controls[index].controls.taskregulationid.value;
    subtaskRegulation = control.controls[index].controls.childtemplatetask
    for(j=0; j<subtaskRegulation.length; j++) {
      if (element=='default') {
        subtaskRegulation.controls[j].patchValue({taskregulationid: 'Task Regulation'})
      } 
      if (element=='1') {
      subtaskRegulation.controls[j].patchValue({taskregulationid: '1'})
      }
      if (element=='2') {
        subtaskRegulation.controls[j].patchValue({taskregulationid: '2'})
        }
      }
  }

    onSubmit() {
    console.log(this.templateForm.value);
    return this.http.post(this._url, [this.templateForm.value])
    .subscribe(
      (data:any) => {
        console.log(data)
      }
    )
  }

}
