import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TemplatesService {

  constructor(private http: HttpClient) { }

  private _urlWorksheet = "https://haofu.airbusdigital.com/api/hof/template/worksheet";
  private _urlPartNumber = "https://haofu.airbusdigital.com/api/hof/template/worksheet-part";
  private _urlTaskType = "https://haofu.airbusdigital.com/api/hof/template/worksheet-tasktype";

  private _urlWorksheetJobcard = "https://haofu.airbusdigital.com/api/hof/template/worksheet-jobcard";
  private _urlJobcard = "https://haofu.airbusdigital.com/api/hof/template/jobcard"

  private _urlJobcardTask = "https://haofu.airbusdigital.com/api/hof/template/jobcard-task";
  private _urlTask = "https://haofu.airbusdigital.com/api/hof/template/task";

  
  getTemplates(): Observable<any>{
    return this.http.get(this._urlWorksheet)
    .pipe(
      map( response => {
        console.log(response)
        return response;
      }),
      catchError( (err) => { 
        console.log(err)
        return throwError(err);
      })
    );
  }

  getTemplateData(params):Observable<any> {
    return this.http.get(this._urlWorksheet+'?templateworksheetcode='+params)
    .pipe(
      map( response => {
        return response
      }),
      catchError( (err) => {
        return throwError(err)
      })
    )
  }

  getJobcard(id): Observable<any> {
    return this.http.get(this._urlJobcard+'?templatejobcardid='+id)
    .pipe(
      map( response => {return response}),
      catchError( ( err) => {return throwError(err)})
    );
  }

  getTask(id): Observable<any> {
    return this.http.get(this._urlTask+'?templatetaskid='+id)
    .pipe(
      map( response => {return response}),
      catchError( ( err) => {return throwError(err)})
    );
  }

  deletePartNumber(id) {
    // console.log(this._urlPartNumber+'/'+id)
    return this.http.delete(this._urlPartNumber+'/'+id)
    .pipe(
      map( response => {
        return response;
      }),
      catchError( (err) => { 
        console.log(err)
        return throwError(err);
      })
    );

  }

  deleteTaskType(id) {
    // console.log(this._urlTaskType+'/'+id)
    return this.http.delete(this._urlTaskType+'/'+id)
    .pipe(
      map( response => { return response;}),
      catchError( (err)=> {
        console.log(err)
        return throwError(err);
      })
    );
  }

  removeTask(id){
    // console.log(this._urlJobcardTask+'/'+id)
    return this.http.delete(this._urlJobcardTask+'/'+id)
    .pipe(
      map( response => { return response;}),
      catchError( (err)=> {
        console.log(err)
        return throwError(err);
      })
    );
  }

  deleteTask(id){
    // console.log(this._urlTask+'/'+id)
    return this.http.delete(this._urlTask+'/'+id)
    .pipe(
      map( response => { return response;}),
      catchError( (err)=> {
        console.log(err)
        return throwError(err);
      })
    );
  }

  removeJobcard(id){
    // console.log(this._urlWorksheetJobcard+'/'+id)
    return this.http.delete(this._urlWorksheetJobcard+'/'+id)
    .pipe(
      map( response => { return response;}),
      catchError( (err)=> {
        console.log(err)
        return throwError(err);
      })
    );
  }

  deleteJobcard(id){
    return this.http.delete(this._urlJobcard+'/'+id)
    .pipe(
      map( response => { return response;}),
      catchError( (err) => {
        console.log(err)
        return throwError(err);
      })
    );
  }

  deleteTemplate(id) {
    return this.http.delete(this._urlWorksheet+'/'+id)
    .pipe(
      map( response => { return response;}),
      catchError( (err) => {
        console.log(err)
        return throwError(err);
      })
    );
  }

}
